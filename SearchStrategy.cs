﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LV7
{
    abstract class SearchStrategy
    {
        public abstract int Search(double number, double[] arr);
    }
}
