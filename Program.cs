﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LV7
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] arr = new double[] { 15, 1, 64, 18, -24, 32, 100.1, -25.55, 100, 204 };
            NumberSequence sequencebubble = new NumberSequence(arr);
            NumberSequence sequencecomb= new NumberSequence(arr);
            NumberSequence sequencesequential = new NumberSequence(arr);
            BubbleSort bubble = new BubbleSort();
            CombSort comb = new CombSort();
            SequentialSort sequential = new SequentialSort();
            sequencebubble.SetSortStrategy(bubble);
            sequencecomb.SetSortStrategy(comb);
            sequencesequential.SetSortStrategy(sequential);

            Console.WriteLine("Before:\n" + sequencebubble.ToString());
            sequencebubble.Sort();
            Console.WriteLine("After bubblesort:\n"+sequencebubble.ToString());
            sequencecomb.Sort();
            Console.WriteLine("After combsort:\n" + sequencecomb.ToString());
            sequencesequential.Sort();
            Console.WriteLine("After sequentialsort:\n" + sequencesequential.ToString());

            LinearSearch linearSearch = new LinearSearch();
            sequencebubble.SetSearchStrategy(linearSearch);
            Console.WriteLine("Number you searched is on place number:");
            Console.WriteLine(sequencebubble.Search(1));
            Console.ReadLine();
        }
    }
}
